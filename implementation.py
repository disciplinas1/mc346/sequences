"""
Find the longest sequences of string combinations.

A combination is defined by the union of two strings
that have a common substring in the beggining and in the end.
The substring must be at least 4 characters long and is
represented only once in the combinated result.
"""

from itertools import permutations, chain

MIN_SUBSTRING_LEN = 4

def max_connection_len(fst, snd):
    """Return max connection length possible."""
    size = len(fst)
    for index in range(size + 1):
        if snd.startswith(fst[index:]):
            return size - index

def join_ordered_strings(fst, snd):
    """Join strings in order. If not possible, return None."""
    size = max_connection_len(fst, snd)
    if size < MIN_SUBSTRING_LEN:
        return None

    return fst + snd[size:]

def join_all_strings(strings):
    """Try to join all strings in a chain."""
    final_string = strings[0]
    for word in strings[1:]:
        final_string = join_ordered_strings(final_string, word)
        if final_string is None:
            return None

    return final_string

def all_permutations(sequence):
    """Get all possible permutations."""
    return chain(*(permutations(sequence, size) for size in range(len(sequence), 0, -1)))

def check_permutation(permutation):
    """
    Check if the strings can all be joined in a chain
    and return the final length. If not possible, return 0.
    """
    joined = join_all_strings(permutation)
    return len(joined) if joined is not None else 0

def longest_valid_permutation(inputs):
    """
    Get the longest possible permutation of strings in inputs
    and return them in order.
    """
    permutations = all_permutations(inputs)
    return max(permutations, key=check_permutation)

def all_longest_chains(inputs):
    """Find the longest sequences of string combinations."""
    words = inputs
    results = list()
    while words:
        longest_permutation = longest_valid_permutation(words)
        for word in longest_permutation:
            words.remove(word)

        results.append(join_all_strings(longest_permutation))

    return results

def read_inputs():
    """Read input strings."""
    inputs = []
    while True:
        try:
            inputs.append(input())
        except EOFError:
            return inputs

def main():
    inputs = read_inputs()
    for result in all_longest_chains(inputs):
        print(result)

if __name__ == '__main__':
    main()
